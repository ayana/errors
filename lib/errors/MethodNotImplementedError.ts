import { GenericError } from './GenericError';

/**
 * Error used when a method or funciton is not (yet) implemented.
 */
export class MethodNotImplementedError extends GenericError {
	public constructor() {
		super('Method not implemented');
	}
}
