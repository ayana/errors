import { IOError } from './IOError';

/**
 * Error used when a network access goes wrong.
 */
export class NetworkError extends IOError {}
