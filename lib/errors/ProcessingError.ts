import { GenericError } from './GenericError';

/**
 * Error used when processing of anything goes wrong.
 */
export class ProcessingError extends GenericError {}
