import { GenericError } from './GenericError';

/**
 * Error used when something has been illegally accessed.
 */
export class IllegalAccessError extends GenericError {}
