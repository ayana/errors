import { IOError } from './IOError';

/**
 * Error used when a file system access goes wrong.
 */
export class FileSystemError extends IOError {
	public constructor(file: string, msg = 'Failed to do file operation') {
		super(`${msg}: ${file}`);
	}
}
