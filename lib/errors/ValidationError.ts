import { ProcessingError } from './ProcessingError';

/**
 * Error used when validating something fails.
 */
export class ValidationError extends ProcessingError {}
