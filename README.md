@ayanaware/errors [![NPM](https://img.shields.io/npm/v/@ayanaware/errors.svg)](https://www.npmjs.com/package/@ayanaware/errors) [![Discord](https://discordapp.com/api/guilds/508903834853310474/embed.png)](https://discord.gg/eaa5pYf) [![Install size](https://packagephobia.now.sh/badge?p=@ayanaware/errors)](https://packagephobia.now.sh/result?p=@ayanaware/errors)
===

Common AyanaWare errors.

If you are developing a library use one of the following patterns:

#1 Use this package as a peer dependency so the package user has to install it to prevent version conflicts in the first place

#2 Use this package as a regular dependency but export the guards from `@ayanaware/errors/guards` so the package user thinks they are coming from your library. i.e. export them on your main class (ex. `MyPackage`) so the package user can use `MyPackage.isGenericError(someError)`

Links
---

[GitLab repository](https://gitlab.com/ayanaware/errors)

[NPM package](https://npmjs.com/package/@ayanaware/errors)

License
---

Refer to the [LICENSE](LICENSE) file.
